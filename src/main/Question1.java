package main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;


public class Question1 {

	public static final String FILE_NAME = "Paragraph.txt";
	
	public static void main(String[] args) {
	
        File file = new Question1().getFileFromResources(FILE_NAME);
        try {
        	// Read string from file
        	String paragraph = new String (Files.readAllBytes(file.toPath()),Charset.forName("UTF-8")); // if not specified, uses windows-1552 on that platform
        	
        	// Abracadabra
        	processParagraph(paragraph);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}
	
	/**
	 * Get file from resources
	 * @param fileName
	 * @return File
	 */
	private File getFileFromResources(String fileName) {

        ClassLoader classLoader = getClass().getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }
	
    /**
     * Generate a count of the occurrences of each word for the text found in the string paragraph.
     * Output a list of words and counts in descending count order (word with highest count listed first).
     * 
     * @param paragraph
     * 
     */
    private static void processParagraph(String paragraph) {
		
    	// Clean up string and remove any alphabets or numbers. with an exception of - and ' ' 
		paragraph = paragraph.replaceAll("[^A-Za-z0-9 -]", "");

		// Create an array of words
		String[] words = paragraph.split(" ");
		
		// Java 8 Magic
		Map<String, Integer> wordsMap = new HashMap<String, Integer>();
		
		for ( String word : words) {
			if (wordsMap.get(word) == null) {
				wordsMap.put(word, 1); 
			} else {
				wordsMap.computeIfPresent(word, (k, v) -> v + 1);
			}
		}

		// Sort Hashmap w.r.t value in reverse order ( Higher value first )
        Stream<Map.Entry<String,Integer>> sortedWordsMap = wordsMap.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()));
        
        sortedWordsMap.forEach(System.out::println);
        
    }

}
